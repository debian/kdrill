extern void MakeSearchPopup();

/* These are for the TOP section of the search window, aka searchnumbers[] */
enum {G_LABEL,G_INPUT,F_LABEL,F_INPUT,POUND_LABEL,POUND_INPUT,
	H_LABEL,H_INPUT,N_LABEL,N_INPUT,U_LABEL,U_INPUT,
	INUSEFILE, SHOWUSEFILE, RADLIST_L, RADLIST_D,
	NUM_OF_N };

/* These are for the normal user input stuff, aka searchwidgets[] */
enum {
	NFORM_W, KANA_W, ENGLISH_W,
	SEARCH_ENG_L, SEARCH_ENG_W,
	SEARCH_KANJI_L, SEARCH_SKIP_W,
	SEARCH_STROKE_W,
	SEARCH_RADICAL_W,
	SEARCH_KANJIPASTE,
	KANA_FORM,
	SEARCH_KANA_L,SEARCH_KANA_B, SEARCH_KANA_W,
	SEARCH_ACTIVEFILTER, SEARCH_STARTTOG, 
	NUM_OF_W};


extern Widget searchwidgets[NUM_OF_W];
extern Widget searchnumbers[NUM_OF_N];

extern void MakeSearchPopup();
extern void printsearch(TRANSLATION);
extern void UpdateSearchlabels();
extern void SearchCallback(Widget,XtPointer,XtPointer);


extern Widget kanainput_popup;
extern Widget search_popup;
extern Widget searchstatusline;
extern Widget romajiinput;

extern void process_kinput(XChar2b);
extern void ksearch_toggles(Widget, XtPointer, XtPointer);
extern void SearchRefresh();


/* The following is stuff in kanasearch.h */
/* It doesn't deserve its own .h file */
/* Besides which, it is tightly linked to the stuff in search.c */

extern void MakeKanainputPopup();

extern void Showinputkana(Widget, XtPointer client_data, XtPointer call_data);

extern XChar2b paragraphglyph[2];/* thing used for "return" symbol on popup */


#define MAXKANALENGTH 17
/* kanastring is the string held by the "type romaji here" widget. */
extern XChar2b kanastring[MAXKANALENGTH];

/* For searches, insist on match from start of word */
extern int match_onlyatstart;
/* For searches, on use "active" (ie: 'would show in guess output') */
extern int match_onlyactive;

/* no point in making this more than once, so share it */
extern Cursor textcursor;
