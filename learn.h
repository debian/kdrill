/* For the "learn" window [stuff in learn.c] */


/* Pops up/down the "learn" window */
extern void LearnCallback(Widget,XtPointer,XtPointer);
extern void MakeLearnPopup();

/* refresh Learned character display*/
extern void DisplayLearnChar();

/* keyboard accelerator hook */
extern void LearnNewChar(Widget w,XtPointer client_data, XtPointer call_data);
extern Widget learn_popup;
