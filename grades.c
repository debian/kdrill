/* "grades.c"
 *	has code for selecting grade levels,
 *	PLUS init for the showinorder stuffs
 */

#include <stdio.h>

#include <ctype.h>
#include <Intrinsic.h>
#include <StringDefs.h>
#include <Xaw/Command.h>
#include <Xaw/Label.h>
#include <Xaw/Form.h>
#include "defs.h"
#include "externs.h"
#include "game.h"
#include "grades.h"
#include "options.h"
#include "utils.h"

/* [0] is "all" button */
Widget gradeButtons[8];
int gradelevelflags;




/* printgrades:
 *	update grade widgets, cause the stupid things don't seem to want
 *	to start as they SHOULD start. Grrr...
 *	CANNOT CALL THIS from a grade button callback, due to
 *	BUG in Xaw.so.6
 */

void printgrades(){
	int buttonloop;
	for(buttonloop=1;buttonloop<8;buttonloop++){
		if(gradelevelflags & (1<<buttonloop)){
			SetWhiteOnBlack(gradeButtons[buttonloop]);
		} else {
			SetBlackOnWhite(gradeButtons[buttonloop]);
		}
	}
}

/* parsegrades:
 *  initialize gradelevelflags, according to ascii string
 *	(containing single word)
 *	If string has no acceptable grade levels, we will
 *	set to ALL grades.
 */
void parsegrades(char *gradestring){
	char *parse = gradestring;
	gradelevelflags = 0;

	while(*parse && (!isspace(*parse )) ){
	    switch(*parse){
		case '1': case '2':
		case '3': case '4':
		case '5': case '6':
			gradelevelflags |= (1 << (*parse -  '0') );
			break;
		case '+':
			gradelevelflags |= ABOVESIXGRADE;
			break;
	    }
	    parse++;
	}

	if(gradelevelflags == 0)
		gradelevelflags = ALLGRADES;
}


/* routine for GradeCallback.
 * This gets called whenever we CHANGE our grade selections.
 * We then update our preference database
 */
void calcgradestring() {
	char gradestring[10];
	gradestring[0]='\0';

	/* Yes, this is messed up. oh well, it works */
	if(gradelevelflags&0x02) strcat(gradestring,"1");
	if(gradelevelflags&0x04) strcat(gradestring,"2");
	if(gradelevelflags&0x08) strcat(gradestring,"3");
	if(gradelevelflags&0x10) strcat(gradestring,"4");
	if(gradelevelflags&0x20) strcat(gradestring,"5");
	if(gradelevelflags&0x40) strcat(gradestring,"6");
	if(gradelevelflags&0x80) strcat(gradestring,"+");

	SetXtrmString("gradelevel",gradestring);
}
/* GradeCalback:
 *	calback for different grade buttons
 */
void GradeCallback(Widget w,XtPointer data,XtPointer calldata){
	char statusbuff[100];
	char statusprefix[100];
	int grademask;
	grademask = 1 << (int) data;
	
	switch((int) data){
		case 1:	case 2:
		case 3:	case 4:
		case 5: case 6:
		case 7: /* + */
			/* already on? */
			if(gradelevelflags & grademask){
				gradelevelflags -= grademask;
				if(! gradelevelflags) {
					setstatus("Must have at least one grade level!");
					Beep();
					gradelevelflags |= grademask;
					return;
				} else {
					CountKanji();
					if(numberofkanji < HAVE_AT_LEAST ) {
						setstatus("Too few kanji");
						gradelevelflags |= grademask;
						Beep();
						CountKanji();
						return;
						
					}
					ReverseButton(w);
					if((int) data <=6)
						sprintf(statusprefix,"Grade %d unset: ",(int) data);
					else
						sprintf(statusprefix,"Grade + unset: ");
				}
				/* CountKanji already called above */
                   	} else {
			/* nope.. turn on grade level */
				ReverseButton(w);
				gradelevelflags |= grademask;
					if((int) data <=6)
						sprintf(statusprefix,"Grade %d set: ",(int) data);
					else
						sprintf(statusprefix,"Grade + set: ");
			}
			CountKanji();
			break;

		case 0:/* "All" button */
			gradelevelflags = ALLGRADES;
			printgrades();
			sprintf(statusprefix,"All grades selected: ");
			CountKanji();
			break;
	}
	sprintf(statusbuff,"%s%d kanji active",statusprefix,numberofkanji);
	calcgradestring();/* update preferences */
	setstatus(statusbuff);
}

