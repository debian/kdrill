#include "kanjisearch.h"

#define NUM_OF_KW 100
extern XChar2b std_translations[NUM_OF_KW][2];


extern TRANSLATION lastsearch;	/* holds index of last kanji searched */

extern void split_kanji_search();
/* Finds all matchs to a particular kanji string */
extern void findkanjiall(XChar2b *);
extern int matchkanji(XChar2b *string,XChar2b*fragment);

extern XChar2b * kstrchr(XChar2b *string, XChar2b kchar);


extern void findunicodestring(XChar2b *target);
extern TRANSLATION dounicodefind(XChar2b *string);

extern void dostrokefind(BYTE strokecount, BITS16 skipval);
extern TRANSLATION doenglishfind();
extern void dousefilefind();
extern void doskipfind(int target);
