#ifndef _KANJISEARCH_H
#define _KANJISEARCH_H

extern void MakeKanjiinputPopup();
extern void MakeSKIPInputPopup();
extern void dokanjifind(int);
extern void Showinputkanji(Widget, XtPointer client_data, XtPointer call_data);
extern void ShowSKIP(Widget, XtPointer client_data, XtPointer call_data);



#endif /* _KANJISEARCH_H */
