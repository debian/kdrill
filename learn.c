/* This is the code for the "Learn" window.
 * The "main" kanjidrill window is for drilling.
 * THIS window, however, is purely for flashcard purposes.
 * It respects the grade and usefile limitations currently in effect
 * It shows the kanji, kana, and english reading of a char/phrase for
 * as long as the user likes, until they press next, or close.
 */



#include <stdio.h>

#include <Xos.h>
#include <Intrinsic.h>
#include <StringDefs.h>
#include <Xfuncs.h>

#include <Shell.h>

#include <Xaw/Command.h>
#include <Xaw/Label.h>
#include <Xaw/Form.h>
#include <Xaw/Box.h>
#include <Xaw/AsciiText.h>

#include "defs.h"
#include "init.h"
#include "utils.h"
#include "externs.h"
#include "game.h"
#include "convert.h"
#include "readfile.h"
#include "searchwidgets.h"
#include "multikanji.h"
#include "log.h"

Widget learn_popup; /* global, so Accelerator can check on it */

static Widget learn_close, learn_randnext,learn_next,learn_usefile;

static Widget learn_kanji,learn_kana,learn_english;

static TRANSLATION nowshowing=NULL;
int showindex=0;

/* This gets called after clicking on the "search" button, or
 *  using the keyboard accelerator. It pops up the search window.
 */
void LearnCallback(Widget w,XtPointer client_data, XtPointer call_data)
{
	static int learn_up = -1; 
	static Position rel_x,rel_y;
	Position x,y;

	/* sometimes we are called with w==NULL by hand */

	if(learn_up==-1){
		/* first time init.. */
		rel_x = GetXtrmNumber("learn_popupx","Learn_popupx");
		rel_y = GetXtrmNumber("learn_popupy","Learn_popupy");
		setup_deletewindow(learn_popup);
		learn_up=0;
	}
	if(isMapped(learn_popup)==False){
		/* Map in! */

		XtTranslateCoords(toplevel,rel_x,rel_y,&x,&y);
		/* it seems some window managers let you have extreme
		 * negative coordinates for pop-ups. sigh
		 */
		if(x<0) x=0;
		if(y<0) y=0;

		XtVaSetValues(learn_popup,
		      XtNx,x,
		      XtNy,y,
		      NULL);

		XtMapWidget(learn_popup);
		setstatus("Bringing up learn window...");

	} else {
		XtUnmapWidget(learn_popup);
	}
}


/* refresh "learn" window with latest char.
 * Routine kept separate, so we can externally toggle between
 * romaji/kana display
 */
void DisplayLearnChar()
{
	if(nowshowing==NULL) return;


	/* This shouldnt be neccessary. It should be redundant with
	 * the creation args. But if I dont do this, and SEPARATELY,
	 * it doesnt display the kanji chars under solaris! :-(
	 * 
	 */

	XtVaSetValues(learn_kanji,
	      XtNencoding, XawTextEncodingChar2b,
	      XtNfont,largekfont,
		      NULL);

	XtVaSetValues(learn_kanji,XtNlabel,nowshowing->kanji, NULL);

	if(romajiswitch==1){
		char stringbuff[MAXROMAJI+1];
		/* translate all kana into romaji */
		/* translate to buffer, then set widget string*/
		kanatoromaji(nowshowing->pronunciation, stringbuff);

		XtVaSetValues(learn_kana,
		      XtNencoding,XawTextEncoding8bit,
		      XtNfont,englishfont,
		      NULL);
		XtVaSetValues(learn_kana,
		      XtNlabel,stringbuff,
		      NULL);

	} else {
		XtVaSetValues(learn_kana,
		      XtNencoding, XawTextEncodingChar2b,
		      XtNfont,smallkfont,
		      NULL);
		XtVaSetValues(learn_kana,
		      XtNlabel,nowshowing->pronunciation,NULL);
	}

	XtVaSetValues(learn_english, XtNlabel,nowshowing->english, NULL);

	if(InUsefile(showindex)){
		HighlightButton(learn_usefile);
	} else {
		UnhighlightButton(learn_usefile);
	}
}



/* Pick a new char randomly, or in order if client_data==True, and
 * display it on our window.
 */
void LearnNewChar(Widget w,XtPointer client_data, XtPointer call_data)
{
	Boolean save_show=showinorder;
	TRANSLATION save_last=lastpicked;
	if(nowshowing!=NULL) {
		lastpicked=nowshowing;
	}

	/* Okay, the 0xff is ugly: its to avoid an irritating size-of-int
	 * warning in casting
	 */
	showinorder=(Boolean)((int)client_data & 0xff);
	nowshowing=picktruevalue();
	showinorder=save_show;
	lastpicked=save_last;
	showindex=trans_to_index(nowshowing);
	DisplayLearnChar();
}

/* Toggle for 'is char in usefile?*/
void LearnUsefileToggle(Widget w,XtPointer client_data, XtPointer call_data)
{
	if(nowshowing==NULL) return;

	SetUseKanji(showindex, !InUsefile(showindex));
	DisplayLearnChar();
	UpdateSearchlabels();
	RefreshMultiLabels();

	CountKanji();
}


/* This is the only exported "make widgets" routine */
void MakeLearnPopup()
{
	Widget learnform;

	/* Yes, we really want "CreateWidget", NOT "CreateManagedWidget",
	 * Otherwise the initial size gets messed up.
	 */
	learn_popup = XtVaCreateWidget("kdrill_learn",
		shellWidgetClass,
		toplevel,
		/* this SHOULD work, but does not */
		XtNtitle,"inlinetitle",
		XtCTitle,"TestTitle",
		XtNiconName,"learn_win",
		NULL);

	/* If we dont do this, the window appears on startup */
	XtSetMappedWhenManaged(learn_popup,False);

	learnform=XtVaCreateManagedWidget(
			"learnform",
			formWidgetClass,
			learn_popup,
			XtNleft,XawChainLeft,
			XtNright,XawChainRight,
			XtNtop, XawChainTop,
			XtNbottom, XawChainBottom,
			NULL);

	learn_kanji=XtVaCreateManagedWidget("learnkanji",
			labelWidgetClass,
			learnform,
			XtNwidth,FULLWIDTH,
			XtNlabel,"                               ",
			XtNleft,XawChainLeft,
			XtNright,XawChainRight,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
			XtNencoding,XawTextEncodingChar2b,
			XtNfont,largekfont,
			NULL);

	learn_kana=XtVaCreateManagedWidget("learnkana",
			labelWidgetClass,
			learnform,
			XtNwidth,FULLWIDTH,
			XtNlabel,"                               ",
			XtNleft,XawChainLeft,
			XtNright,XawChainRight,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
			XtNencoding,XawTextEncodingChar2b,
			XtNfont,smallkfont,
			XtNfromVert,learn_kanji,
			NULL);
	learn_english=XtVaCreateManagedWidget("learnenglish",
			labelWidgetClass,
			learnform,
			XtNwidth,FULLWIDTH,
			XtNlabel,"Press a 'next' button",
			XtNleft,XawChainLeft,
			XtNright,XawChainRight,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
			XtNfont,englishfont,
			XtNfromVert,learn_kana,
			NULL);

	learn_next= XtVaCreateManagedWidget("learnnext",commandWidgetClass,
			learnform,
			XtNlabel,"Next",
			XtNleft,XawChainLeft,
			XtNright,XawChainLeft,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
			XtNfromVert,learn_english,
			NULL);
	learn_randnext= XtVaCreateManagedWidget("learnnextrand",commandWidgetClass,
			learnform,
			XtNlabel,"Next(Random)",
			XtNleft,XawChainLeft,
			XtNright,XawChainLeft,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
			XtNfromVert,learn_english,
			XtNfromHoriz,learn_next,
			NULL);
	learn_usefile= XtVaCreateManagedWidget("learnusefile",commandWidgetClass,
			learnform,
			XtNlabel,"In usefile",
			XtNleft,XawChainLeft,
			XtNright,XawChainLeft,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
			XtNfromVert,learn_english,
			XtNfromHoriz,learn_randnext,
			NULL);

	learn_close= XtVaCreateManagedWidget("learnclose",commandWidgetClass,
			learnform,
			XtNlabel,"Close",
			XtNleft,XawChainRight,
			XtNright,XawChainRight,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
			XtNfromVert,learn_english,
			XtNfromHoriz,learn_usefile,
#ifdef WOULD_LIKE_TO_DO_THIS
			XtNwinGravity, EastGravity,
#else
			XtNhorizDistance,60,
#endif
			NULL);

	XtAddCallback(learn_next,XtNcallback,LearnNewChar,
		      (XtPointer)True);
	XtAddCallback(learn_randnext,XtNcallback,LearnNewChar,
		      (XtPointer)False);
	XtAddCallback(learn_usefile,XtNcallback,LearnUsefileToggle,NULL);
	XtAddCallback(learn_close,XtNcallback,LearnCallback,NULL);

	UnhighlightButton(learn_usefile);
	
}


