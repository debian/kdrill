/* This file makes the windows for kanji search */
/* Actual searcing happens in search.c */

#include <stdio.h>

#include <Xos.h>
#include <Xfuncs.h>
#include <Intrinsic.h>
#include <StringDefs.h>

#include <Shell.h>

#include <Xaw/Command.h>
#include <Xaw/Label.h>
#include <Xaw/Form.h>
#include <Xaw/Box.h>
#include <Xaw/AsciiText.h>

#include "defs.h"
#include "externs.h"

#include "searchwidgets.h"
#include "kanjisearch.h"
#include "convert.h"
#include "game.h"
#include "utils.h"
#include "init.h"

/***************************************************/
/* First, "four corners" method. Others lower down */
/***************************************************/


Widget kanjiinput_popup;
static Widget kanjiwidgets[14];
static Widget kanjiquarters[4];


static int quarter_up=-1;
XChar2b quarterchars[11][2] =
{
	{{0x28, 0x35}, {0, 0}},
	{{0x28, 0x2c}, {0, 0}},
	{{0x28, 0x2d}, {0, 0}}, /* vertical */
	{{0x21, 0x46}, {0, 0}}, /* 'dot' */
	{{0x28, 0x36}, {0, 0}}, /* cross */
	{{0x25, 0x2d}, {0, 0}}, /* ki */
	{{0x25, 0x6d}, {0, 0}}, /* square */
	{{0x28, 0x2e}, {0, 0}}, /*angle (needs replacement) */
	{{0x25, 0x4f}, {0, 0}},
	{{0x3e, 0x2e}, {0, 0}}, /*chisai*/
	{{0x21, 0x21}, {0, 0}}, /*fake zero*/
};

/* select a particular quarter for the "four corners" search method */
/* mark our internal counter, plus make that one "selected" */
static void selectquarter(Widget w, XtPointer data, XtPointer call_data)
{
	int num;
	int kcount;

	num = (int)data;
	
	for(kcount=0; kcount<4; kcount++){
		if(num == kcount)
			HighlightButton(kanjiquarters[kcount]);
		else
			UnhighlightButton(kanjiquarters[kcount]);
	}
	quarter_up = num;
	
}

/* change the active quarter for the "four corners" method, to
 * the symbol just clicked on
 */
void changequarter(Widget w, XtPointer data, XtPointer call_data)
{
	if(quarter_up == -1){
		puts("internal error: changequarter called with no quarter valid");
		return;
	}
	XtVaSetValues(kanjiquarters[quarter_up],
		      XtNlabel, quarterchars[(int)data],
		      NULL);
}

/* we have to go indirectly to "dokanjifind" because
 * we first have to gather the "four corner" information from the
 * widgets, and put it into a single number
 * The actual search happens in dokanjifind()
 */
void DoKanjiFind(Widget w, XtPointer data, XtPointer call_data)
{
	int kcount, cornercount;
	XChar2b *labelval;
	int searchnumber = 0;

	for(cornercount=0; cornercount<4; cornercount++){
		XtVaGetValues(kanjiquarters[cornercount],
			      XtNlabel, &labelval,
			      NULL);
		
		for(kcount=0; kcount<=10; kcount++){
			if(bcmp(labelval, quarterchars[kcount],2) == 0)
				break;
		}
		if(kcount> 10){
			fprintf(stderr, "ERROR! impossible overrun for DoKanjiFind\n");
			exit(1);
		}
		/* for fake "zero" */
		if(kcount==10)
			kcount=0;

		switch(cornercount){
			case 0:
				searchnumber = kcount * 1000;
				break;
			case 1:
				searchnumber += kcount * 100;
				break;
			case 2:
				searchnumber += kcount * 10;
				break;
			case 3:
				searchnumber += kcount ;
				break;
		}
	}
#ifdef DEBUG
	printf("searching for Q value %d\n", searchnumber);
#endif
	dokanjifind(searchnumber);
}


void addHelpText(Widget parent, Widget displaything)
{
	XCharStruct charstruct = englishfont->max_bounds;

	/*Widget helptext=*/ XtVaCreateManagedWidget("4kanjihelp",asciiTextWidgetClass,
			parent,	XtNdisplayCaret, False,
			XtNfromVert, displaything,
			XtNvertDistance, 20,
			XtNfont, englishfont,
			XtNwidth, 46*charstruct.width,
			XtNheight, 14*(charstruct.ascent+charstruct.descent),
			XtNtop, XawChainBottom,
			XtNbottom, XawChainBottom,
			XtNleft, XawChainLeft,
			XtNright, XawChainLeft,
			XtNstring,
"This is the \"Four corner\" search method.\n"
"The four squares in the center represent\n"
"the four corners of the kanji.\n"
"Press each corner in turn, then pick which\n"
"of the ten shapes at the top most closely\n"
"match that corner of the kanji you are\n"
"trying to find. Unfortunately, this is not\n"
"always as simple as it seems. For example,\n"
"if a shape in the left corner extends to\n"
"the right corner, choose the shape for the\n"
"left, but choose blank for the right corner.\n"
"You should read kanjidic.doc for details.\n"
"  Press the backwards |P to search.",
			NULL);

}

/* make the widgets for the "four quarter" kanji search method */
void makekanjiinput(Widget parent)
{
	int kcount;
	Widget displaything;
	Widget search;

	for (kcount=0; kcount<11; kcount++){
		char namestr[10];
		sprintf(namestr, "%d", kcount);
		kanjiwidgets[kcount] =
			XtVaCreateWidget(namestr,
				commandWidgetClass,
				parent,
				XtNlabel, quarterchars[kcount],
				XtNencoding, XawTextEncodingChar2b,
				XtNfont, smallkfont,
				XtNtop, XawChainTop,
				XtNbottom, XawChainTop,
				XtNleft, XawChainLeft,
				XtNright, XawChainLeft,
				NULL);
		if((kcount>0) && (kcount <10)){
			XtVaSetValues(kanjiwidgets[kcount],
				      XtNfromHoriz,
				      kanjiwidgets[kcount-1],
				      NULL);
		}
		XtAddCallback(kanjiwidgets[kcount],
			      XtNcallback, changequarter,
			      (XtPointer) kcount);
	}

	XtVaSetValues(kanjiwidgets[10],
			      XtNfromVert,
			      kanjiwidgets[0],
			      NULL);

	displaything =
		XtVaCreateManagedWidget("displayfourkanji", formWidgetClass,
				parent,
				XtNfromVert, kanjiwidgets[0],
				XtNfromVert, kanjiwidgets[9],
				XtNfromHoriz, kanjiwidgets[3],
				XtNleft, XawChainLeft,
				XtNtop, XawChainTop,
				XtNright, XawChainLeft,
				XtNbottom, XawChainTop,
				XtNvertDistance, 20,
				
				NULL);
	kanjiquarters[0] = XtVaCreateWidget("upleft", commandWidgetClass,
				displaything, XtNlabel, quarterchars[0],
				XtNencoding, XawTextEncodingChar2b,
				XtNleft,XawChainLeft,
				XtNright,XawChainLeft,
				XtNtop, XawChainTop,
				XtNbottom, XawChainTop,
				XtNfont, smallkfont, NULL);
	kanjiquarters[1] = XtVaCreateWidget("upright", commandWidgetClass,
				displaything, XtNlabel, quarterchars[0],
				XtNencoding, XawTextEncodingChar2b,
				XtNfromHoriz, kanjiquarters[0],
				XtNright,XawChainRight,
				XtNleft,XawChainRight,
				XtNtop, XawChainTop,
				XtNbottom, XawChainTop,
				XtNfont, smallkfont, NULL);
	kanjiquarters[2] = XtVaCreateWidget("downleft", commandWidgetClass,
				displaything, XtNlabel, quarterchars[0],
				XtNencoding, XawTextEncodingChar2b,
				XtNfromVert, kanjiquarters[0],
				XtNleft,XawChainLeft,
				XtNright,XawChainLeft,
				XtNtop, XawChainBottom,
				XtNbottom, XawChainBottom,
				XtNfont, smallkfont, NULL);
	kanjiquarters[3] = XtVaCreateWidget("downright", commandWidgetClass,
				displaything, XtNlabel, quarterchars[0],
				XtNencoding, XawTextEncodingChar2b,
				XtNfromVert, kanjiquarters[0],
				XtNfromHoriz, kanjiquarters[2],
				XtNright,XawChainRight,
				XtNleft,XawChainRight,
				XtNtop, XawChainBottom,
				XtNbottom, XawChainBottom,
				XtNfont, smallkfont, NULL);

	/* This is a little PParagraph symbol the user can click on,
	 * to force the search
	 */
	search = XtVaCreateManagedWidget("dosearch", commandWidgetClass,
				parent,
				XtNencoding, XawTextEncodingChar2b,
				XtNfont, smallkfont,
				XtNfromVert, displaything,
				XtNfromVert, kanjiwidgets[9],
				XtNvertDistance, 50,
				XtNfromHoriz, kanjiwidgets[8],
				XtNright,XawChainRight,
				XtNleft,XawChainRight,
				XtNtop, XawChainTop,
				XtNbottom, XawChainTop,
				
				XtNlabel, paragraphglyph,
				NULL);
				  
	XtManageChildren(kanjiwidgets, 11);
	XtManageChildren(kanjiquarters, 4);
	

	addHelpText(parent, displaything);

	for(kcount=0; kcount<4; kcount++){
		XtAddCallback(kanjiquarters[kcount],
			      XtNcallback, selectquarter,
			      (XtPointer) kcount);
	}
	XtAddCallback(search, XtNcallback, DoKanjiFind, (XtPointer) NULL);

}

/* exported routine */
void MakeKanjiinputPopup()
{
	Widget kanjiinputform;

	kanjiinput_popup = XtVaCreatePopupShell("kdrill_kanjisearch ",
		topLevelShellWidgetClass,
		search_popup,
		NULL);

	kanjiinputform = XtVaCreateManagedWidget("kanjiinputform",
					     formWidgetClass,
					     kanjiinput_popup,
					     NULL);

	/* make the 10 input things, plus 4 display things */ 
	makekanjiinput(kanjiinputform);
	/* and now set the correct buttons to initially be sensitive */
	selectquarter(NULL, 0, NULL);

}

/* This gets called after clicking on the "Kanji search" button.
 * It pops up the kanji fourcorner input window
 */
void Showinputkanji(Widget w,XtPointer client_data, XtPointer call_data)
{
	static int kanji_up = -1;

	static Position rel_x,rel_y;
	Position x,y;


	if(kanji_up==-1){
		/* first time init.. */
		rel_x = 10;
		rel_y = 10;

		kanji_up=0;
	}
	if(isMapped(kanjiinput_popup)==False){
		XtTranslateCoords(search_popup,rel_x,rel_y,&x,&y);
		XtVaSetValues(kanjiinput_popup,
		      XtNx,x,
		      XtNy,y,
		      NULL);
		XtPopup(kanjiinput_popup,XtGrabNone);
		if(kanji_up==0){
			setup_deletewindow(kanjiinput_popup);
			kanji_up=1;
		}
		setstatus("Bringing up kanji input window...");
	} else {
		XtPopdown(kanjiinput_popup);
	}
}



/* SKIP methods moved to skipsearch.c */
