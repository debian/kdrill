
/* char* passed in to kanatoromaji better be at least MAXROMAJI long 
 * Kinda silly to have this be 200 chars long, since user will probably
 * never see it. But what the heck, this avoids printing out warnings
 * from kanatoromaji, about truncation
 */
#define MAXROMAJI 200
extern void kanatoromaji(XChar2b *, char *);
void romajitokana(XChar2b *kstart);

extern void Handle_romajikana(Widget, XtPointer, XEvent *, Boolean *);
