/* widgets.h */

extern void DescribeCurrent(TRANSLATION);
extern void TallyWrong();
extern void MakeWidgets();
extern void MakeSearchPopup();
extern void MakeFrequency(Widget parent);

extern XtAccelerators AllAccel;

#ifdef USE_OKU
extern Widget ONWidget;
#endif

extern Widget currentkanjiNum, kanjiMissed;
