
         kdrill v6.0 INSTALL file

[Note: MAKE SURE to use KDrill.ad to update your KDrill resource file
   from older versions!]

[be aware that gcc -O2 may cause coredumps, with cut-n-paste!
  If so, rebuild with -O  ! ]

READ THIS FILE CAREFULLY.


  C O N T E N T S
 =================

  - REQUIREMENTS
  - COMPILING
  - GETTING A FULL DICTIONARY
  - COLOUR OPTIONS
  

REQUIREMENTS:
 kdrill REQUIRES an ANSI-C compiler (And X11R5 libXaw, or better)
 kdrill now requires that you have the "strstr(a,b)" C library call.
   I THINK just about everyone has it nowadays.
   (if you don't, please let me know)
   All it does is check whether string b is in string a.


COMPILING

 You should scan through the top of "Imakefile", for things to tweak before
 compiling.
 For example, if you want it to install in /usr/local/bin, change BINDIR.
 That's probably the only thing you'll want to change.

 Once you have done this, you may then do

 xmkmf
 make
 make install
 make install.man
 cp [Dictionary file] [DICTLOCATION]  # (See next section for details)

 You may also choose to install some or all of the extra .dic files
 in the "makedic" directory, for those people who haven't
 learned hiragana yet. Just 'cp' them as desired to some shared location
 like /usr/local/lib, and specify you want to use them with
 the appropriate command-line flags to kdrill

 ##################################################

GETTING A FULL-SIZE DICTIONARY

 kdrill comes with a REDUCED-size version of the "kanjidic" dictionary,
 that you can copy over to /usr/local/lib. However, you'll probably want
 to get the full version (which is also free)
 Read the "README" file about how to get the full "kanjidic" file, as well
 as the optional "edict" file.

 kdrill also comes with two mini-dictionaries.
 Look in the "makedic" subdirectory for optional .edic files to help
 beginners learn the *basic* characters, before tackling the true kanji chars.

 Currently, there are a few options how to install the default dictionary file.
 Unmodified, kdrill will look for /usr/local/lib/kanjidic.gz,
 The ".gz" means that it expects a gzipped file. 
 I strongly advise you to get gzip, and save yourself some space.
 If not, you can copy the uncompressed file there, but you will have
 to change what kdrill looks for.

 You can change what file it looks for, with "kdrill -kdictfile filename"

 kdrill will check to see if the dictionary is compressed or not, judging by
 whether it has the UNCOMPRESSEXT on the end.  If it is compressed, it will
 attempt to open the file through the uncompression program of your choice.
 If the dictionary is not compressed, it will open the file "normally".


COLOUR OPTIONS

 KDrill now comes slight colored. It uses grey backgrounds (Oooooo!)
 However, you can change that to whatever you prefer.
 You may tune to taste via the KDrill.ad file. "make install" will
 hopefully put it in the right place for you.
 That file is also where you can change global defaults like where the
 dictionary is, if you dont feel like recompiling.

 Note that while the background colors are set in the KDrill resource file,
 most buttons default to whatever colors your system uses. For example, if
 you are running CDE, there is a "Style manager" that lets you change desktop
 colors. 
 If you have a monochrome system (I feel sorry for you) you might want to
 just comment out the color lines in the "KDrill.ad" resource file, if you
 have it installed.  (or not bother to install it at all)

 Note for X newbies:
  "KDrill.ad" is the "resource file", that should be automatically installed as
  "KDrill", in the appropriate directory. "make install" should do that
  for you. If you don't have the permissions required to install it, you can
  put the lines you want from it in ~/.kdrill before starting kdrill.
  Alternatively, you may be able to put them in ~/.Xdefaults.


