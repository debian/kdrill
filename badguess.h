
/* every time a user misses a translation, call this.
 * It will adjust the global numberincorrect, and adjust the
 * translation struct as well.
 * Plus, do some special invisible frequency weighting, for
 * GetRepeat()
 *
 * It will properly call RemoveThis(), if appropriate. 
 */
extern void AdjustBadCache(TRANSLATION);

/* call this if translation incorrect count has gone to zero */
extern void RemoveBadCache(TRANSLATION);

/* This returns the most frequenly misguessed kanji, or
 * NULL, if nothing in cache.
 */
extern TRANSLATION GetRepeat();
