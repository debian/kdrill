/* atributes.c
 *  This file currently exists SOLELY to contain a quickly
 *  portable routine to turn on backing store.
 *  I don't actually use it any more, but this is an old traditional
 *  extra to kdrill, so I'll leave it in :-)
 *  I would NEVER want to turn OFF backing store,
 *  hence there is no UnsetBackingStore function given here:-)
 */
#include <stdio.h>
#include <X11/Xos.h>
#include <X11/Xlib.h>
static XSetWindowAttributes attribs;

int SetBackingStore(display,screen,win)
Display *display;
Screen *screen;
Window win;
{
	if(!DoesBackingStore(screen)){
		fprintf(stderr,"This aplication expects to have backing store available\n");
		return 0;
	}
	attribs.backing_store = Always;
	XChangeWindowAttributes(display,win,CWBackingStore,&attribs);
	return 1;
}
