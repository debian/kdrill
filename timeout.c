/* File to test having X timeouts */

#include <stdio.h>
#include <stdlib.h>

#include <Xlib.h>
#include <Xatom.h>
#include <Xutil.h>
#include <Intrinsic.h>
#include <StringDefs.h>
#include <Xaw/Command.h>
#include <Xaw/Label.h>
#include <Xaw/Form.h>
#include <Xaw/Box.h>
#include <Xaw/AsciiText.h>

#include "defs.h"
#include "externs.h"
#include "utils.h"
#include "game.h"
#include "timeout.h"

/* Handle user input of new timeout length */
static char * timeoutAccel = 
 " <Key>Return:  update-timeout()";

static Widget enablebutton;

static XtIntervalId timeout;
static int in_use=0; /* is timer currently RUNNING*/

/* is timer currently ENABLED, or should we ignore start/stop calls?*/
static int enabled=0;

/* This actually gets overridden in GetOptions()*/
static int timeoutmilis=5000;

/************************************************************/

/* Exists to be a target for when user presses RETURN in
 * timeout number-of-seconds option window
 */
void 
UpdateTimeout(Widget w,XEvent *event,String *params,Cardinal *num_parags){
	int num=GetWidgetNumberval(w);
	setTimeoutLen(num);
	setstatus("timeout value updated");
}

/* callback for "enable/disable" button , and gets used by
 * keyboard accelerator, too
 */
void TimerCallback(Widget w,XtPointer data,XtPointer calldata){
	int newstate=(1 - enabled);
	switch(newstate){
	    case 0:
		StopTimeout();
		setstatus("Timer stopped");
		XtVaSetValues(enablebutton,XtNlabel,"Start timer",NULL);
		break;
	    case 1:
		setstatus("Timer ENABLED. Go to next kanji to start.");
		XtVaSetValues(enablebutton,XtNlabel,"Stop timer",NULL);
		break;
	}
	enabled=newstate;
}

/* Fills out a Form widget with buttons to change timeout values*/
void MakeTimeoutOptions(Widget parentForm){
	XtAccelerators Accel;
	Widget input;
	Widget ttitle, tlabel;
	char numberstring[10];

	sprintf(numberstring,"%.2d",timeoutmilis/1000);

	ttitle=XtVaCreateManagedWidget(
			"timeoutTitle",labelWidgetClass, parentForm,
			XtNlabel,"  Timeout Options",
			XtNborderWidth,0,
			NULL);
	tlabel=XtVaCreateManagedWidget(
			"timeoutLabel",labelWidgetClass, parentForm,
			XtNlabel,"Seconds:",
			XtNfromVert,ttitle,
			XtNborderWidth,0,
			NULL);

	input=XtVaCreateManagedWidget(
			"timeoutInput",asciiTextWidgetClass,parentForm,
			XtNfromVert,ttitle,
			XtNfromHoriz,tlabel,
			XtNeditType,XawtextEdit,
			XtNwidth,30,
			XtNstring,numberstring,
			NULL);

	enablebutton=XtVaCreateManagedWidget(
			"timeoutEnable",commandWidgetClass,parentForm,
			 XtNfromVert,tlabel,
			 XtNlabel,"Start timer",
			 NULL);

	XtAddCallback(enablebutton,XtNcallback,TimerCallback,NULL);

	Accel = XtParseAcceleratorTable(timeoutAccel);
	XtOverrideTranslations(input,Accel);
}


	/*******************************************************/
	/* GUI stuff above line. Control routines below        */
	/*******************************************************/


/* exists so init routines can set the timeout length, mainly.
 * value is in seconds.
 */
void setTimeoutLen(int newlen){
	if(newlen<2){
		setstatus("timeout value too small");
		return;
	}
	if(newlen>99){
		setstatus("timeout value too large");
		return;
	}
	SetXtrmNumber("timersec",newlen);
	timeoutmilis=newlen * 1000;
}


/* returns 0 if stopped before timeout occurred, or timer not enabled
 * return 1 if timeout had already hit
 */
int StopTimeout(){
	if(enabled==0) return 0;

	if(in_use==0){
		return 1;
	}
	XtRemoveTimeOut(timeout);
	in_use=0;
	return 0;
}


void handletimeout(XtPointer closure, XtIntervalId *timerid){
	Beep();
	setstatus("TIME IS UP!");
	cheatcallback(NULL, (XtPointer)1, NULL);
	in_use=0;

}

/* Interval is in milliseconds */
void StartTimeout(){
	if(enabled==0) return;
	if(in_use!=0){
		StopTimeout();
	}

	timeout=XtAppAddTimeOut(Context,timeoutmilis,
				handletimeout,0);
	in_use=1;

}

/* call with 1 to enable timer, 0 to disable.*/
void EnableTimer(int onoff){
	if(onoff==0){
		if(in_use!=0){
			StopTimeout();
		}
	}
	enabled=onoff;
}
