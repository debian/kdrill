Source: kdrill
Section: education
Priority: optional
Maintainer: Євгеній Мещеряков <eugen@debian.org>
Build-Depends: debhelper-compat (= 12), xutils-dev, libxaw7-dev, libx11-dev, libxt-dev, x11proto-core-dev
Standards-Version: 4.5.1
Homepage: http://www.bolthole.com/kdrill/
Vcs-Git: https://salsa.debian.org/debian/kdrill.git
Vcs-Browser: https://salsa.debian.org/debian/kdrill

Package: kdrill
Architecture: any
Recommends: kanadic | kanjidic (>= 2002.02.06-1), xfonts-base (>> 4.0)
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: edict, xjdic
Description: kanji drill and dictionary program
 This package provides a graphical program for learning Japanese
 characters, which also doubles as a dictionary lookup program.
 It requires a dictionary package, such as kanjidic (for learning
 kanji) or kanadic (for learning hiragana or katakana), although
 you can specify a custom dictionary file on the command line.

Package: makedic
Section: text
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: dictionary compiler for KDrill
 This package provides a program to create custom dictionary files for
 KDrill. It can also generate kana dictionary drill files.

Package: kanadic
Architecture: all
Depends: ${misc:Depends}
Suggests: kdrill
Multi-Arch: foreign
Description: katakana and hiragana drill files for KDrill
 This package provides files for practicing katakana and hiragana with
 KDrill. It includes basic and extended versions of each list.
