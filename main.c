
#include <stdio.h>
#include <stdlib.h>

#include <Xlib.h>
#include <Xatom.h>
#include <Xutil.h>
#include <Intrinsic.h>
#include <StringDefs.h>
#include <Xaw/Command.h>
#include <Xaw/Label.h>
#include <Xaw/Form.h>
#include <Composite.h>

#include "defs.h"
#include "externs.h"
#include "init.h"
#include "readfile.h"
#include "game.h"
#include "grades.h"
#include "log.h"
#include "options.h"
#include "radsearch.h"
#include "utils.h"

#include "patchlevel.h"

static char *version = VERSION;

char *homedir=NULL;
Display *display;
int screen;
Window mainwindow,rootwindow;
GC gc,cleargc;
XtAppContext Context;

XChar2b *kstring;
XChar2b onecharstring;

unsigned long white,black;




/* REAL  OS's don't need this. 
 * But because I'm such a nice guy, and recognise that there
 * are screwey OS's out there...
 */
void clean_up_structs(){
	int i;
	for(i=lowestkanji; i<=highestkanji;i++){
		struct translationstruct *T;
		if(translations[i] == NULL)
			continue;
		T = translations[i];

		if(T->english != NULL)
			free(T->english);
		/* for some reason, 0 is better than NULL */
		if(T->pronunciation != 0) 
			free (T->pronunciation);
#ifdef USE_OKU
		if(T->ON_extra != 0)
			free(T->pronunciation);
#endif
	}
}

/* quitbutton, of course */
void quit(Widget w,XtPointer data,XtPointer calldata)
{

#ifdef DEBUG
	puts("quitting cleanly?");
#endif

	/* rely on this to do the right thing in all cases */
	MakeLog(NULL, NULL, NULL);
	SaveUsefile();
	SaveConfig();
	XtCloseDisplay(display);

	/* I supose we'll be nice an' clean up memory somewhat.. */
	clean_up_structs();
	exit(0);
}


void usage(){
	printf(" kdrill    --  %s\n",version);
	fflush(stdout);
	puts("A program to drill on kanji to english, or vica versa");
	puts("");
	puts("Options:");
	puts("  -usefile usefilename         Changes abridgement file of dictionary");
	puts("  -nousefile                   Do not abridge initially");
	puts("  -kdictfile dictfilename       Changes kanji dictionary file from");
	puts("                               \"kanjidic\" to some other file");
	puts("  -edictfile dictfilename       Changes phrase dictionary file from");
	puts("                               \"edict\" to some other file");
	puts("  -englishfont fontname        Changes english button font");
	puts("  -kanjifont Kanjifontname     Changes LARGE kanji font");
	puts("  -smallkanji Kanjifontname    Changes small kanji font");
	puts("  -noBell                      Turns off beep for incorrect answer");
	puts("  -guessmeaning                Starts off with four kanji to one meaning");
	puts("  -notallT                     Allow non-complete dictionary entries.");
	puts("                                  warning: consult man-page.");
	puts("  -showkana                    Start showing kana meanings instead of english");
	puts("  -romajiswitch                Show kana meanings in romaji instead of kana");
	puts("  -showinorder                 Go through kanji in order of #, NOT randomly");
	puts("  -gradelevel                  Define while grades should be used.");
	puts("                                use any or all of \"123456+\"");
	puts("   -lowfrequency               Lowest frequency kanji you wish to see.");
	puts("   -highfrequency              Highest frequency kanji you wish to see");
	puts("                  (\"1\" is HIGH, \"2000\" is low)");
	puts("   -logfile filename           Set logfile to non-standard name");
	puts("");
	puts("       The above options can also be set as resources,");
	puts("       with the same name as their optionflag");
	puts("");
	exit(0);
}


/* debugging routine.
 * Prints out ALL english phrases loaded into memory.
 * Note: This can be many thousands of lines!!!
 */
void dumpenglish()
{
	struct translationstruct *parse;

	parse = translations[lowestkanji];

	while(parse != NULL){
		if(parse->english == NULL){
			printf("NULL english\n");
		} else {
			printf("English: %s\n", parse->english);
		}
		parse = parse->nextk;
	}
}


/*
 * Welcome, Welcome, Welcome!
 */
int main(int argc,char *argv[])
{
	printf("kdrill %s: by Philip Brown -- phil@bolthole.com\n",
	       VERSION);
	puts("Starting up kdrill... please wait a while.");
	fflush(stdout);

	/* Widgets get made down in initstuffs()... */
	/* also, resources get called */
	initstuffs(&argc,argv);

 /*	SetBlackOnWhite(form);*/


	/* check for abridgements of the dictionary... */
	initusefile();
	/* and actually read in structs of kanjidic dictionary */
	readstructs();
	/* now try for 'edict' dictionary as well */
	readedict();

	/* And finally, read in radical file */
	InitRadicals();

	/* Needs to be done after radical file has been read in.
	 * Otherwise, would be in MakeSearchPopup like the other search popups.
	 */
	MakeRadicalinputPopup();

	/*dumpenglish();*/
	
	initlog(); /* old way or new way: same name */

	CountKanji();
	if((numberofkanji<HAVE_AT_LEAST) && useUsefile){
		useUsefile=False;
		UpdateUsefileL();
		CountKanji();
	}
	if(numberofkanji<HAVE_AT_LEAST){

		fprintf(stderr,"\nERROR: There are too few kanji readable in the current configuration\n");
		fprintf(stderr,"Please reconfigure your usefile, raise the grade level,\n");
		fprintf(stderr,"  remove frequency limits, or verify your dictionary file.\n");
		exit(0);
	}

	
	SetupGuess();
	TallyWrong();

	XtAppMainLoop(Context);
	return 0;
}





