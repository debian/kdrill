void		Beep();
int		nextword(unsigned char **);
unsigned char	*nextchar(unsigned char *);
XChar2b		*dup_16(XChar2b *);
void		printline(unsigned char *);
FILE		*open_compressed(char *);
TRANSLATION	FindField(TRANSLATION,
			  Boolean (*func)(TRANSLATION,void *),
			  void *);
int		FindIndex(char *);

int		isapipe(FILE *);

Boolean isMapped(Widget w);

extern void ReverseButton(Widget);
extern void HighlightButton(Widget);
extern void UnhighlightButton(Widget);
extern void SetWhiteOnBlack(Widget);
extern void SetBlackOnWhite(Widget);

extern int GetWidgetNumberval(Widget);
extern int GetWidgetHexval(Widget);
extern int GetWidgetWidth(Widget);

/* general-purpose widget operators.. */
extern void SetWidgetNumberval(Widget,int);
extern void SetWidgetHexval(Widget,int);

